import React from 'react';
import { TopPanel } from './TopPanel';
import './JapaneseCrossword.scss';
import { LeftPanel } from './LeftPanel';
import { MainPanel } from './MainPanel';

export const JapaneseCrossword = ({
  field,
  currentField,
  updateCell,
}: {
  field: number[][];
  currentField: number[][];
  updateCell: (position: { x: number; y: number }) => void;
}): JSX.Element => {
  const numbersX = field.map((line) => {
    const values: number[] = [];
    let prev = 0;
    let currentSum = 0;
    line.forEach((elem) => {
      if (elem === 1) {
        currentSum++;
        prev = 1;
      } else {
        if (prev === 1) {
          values.push(currentSum);
        }
        currentSum = 0;
        prev = 0;
      }
    });
    if (prev === 1) {
      values.push(currentSum);
    }
    if (!values.length) values.push(0);
    return values;
  });

  const numberY = [];

  for (let i = 0; i < field[0].length; i++) {
    const values: number[] = [];
    let prev = 0;
    let currentSum = 0;
    for (let k = 0; k < field.length; k++) {
      const elem = field[k][i];
      if (elem === 1) {
        currentSum++;
        prev = 1;
      } else {
        if (prev === 1) {
          values.push(currentSum);
        }
        currentSum = 0;
        prev = 0;
      }
    }
    if (prev === 1) {
      values.push(currentSum);
    }
    if (!values.length) values.push(0);
    numberY.push(values);
  }

  return (
    <div className="japanese__wrap">
      <div />
      <TopPanel numbers={numberY} />
      <LeftPanel numbers={numbersX} />
      <MainPanel field={currentField} updateCell={updateCell} />
    </div>
  );
};

