import React from 'react';
import { Cell } from './Cell';
import './LeftPanel.scss';
import close from '../../icons/close.svg';

export const MainPanel = ({
  field,
  updateCell,
}: {
  field: number[][];
  updateCell: (position: { x: number; y: number }) => void;
}): JSX.Element => {
  return (
    <div className="leftpanel" style={{ width: 'fit-content' }}>
      {field.map((line, idx) => (
        <div className="leftpanel__line" key={idx}>
          {line.map((value, idx_2) => (
            <Cell
              value={value === 2 ? <img src={close} alt="close" /> : !!value}
              key={idx_2}
              updateCell={() =>
                updateCell({
                  x: idx,
                  y: idx_2,
                })
              }
            />
          ))}
        </div>
      ))}
    </div>
  );
};

