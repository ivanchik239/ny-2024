/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import { Cell } from './Cell';
import './LeftPanel.scss';

export const LeftPanel = ({ numbers }: { numbers: number[][] }): JSX.Element => {
  return (
    <div className="leftpanel">
      {numbers.map((line, idx) => (
        <div className="leftpanel__line" key={idx}>
          {line.map((number, idx_2) => (
            <Cell value={number} key={idx_2} />
          ))}
        </div>
      ))}
    </div>
  );
};

