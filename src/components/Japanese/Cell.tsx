/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import './Cell.scss';
import classNames from 'classnames';

export const Cell = ({
  value,
  updateCell,
}: {
  value: number | boolean | JSX.Element;
  updateCell?: () => void;
}): JSX.Element => {
  return (
    <div
      className={classNames(
        'cell',
        value === true && 'cell__colored',
        updateCell && 'cell__pointer',
      )}
      onClick={() => {
        updateCell?.();
      }}
    >
      {value === true ? '' : value}
    </div>
  );
};

