/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import { Cell } from './Cell';
import './TopPanel.scss';

export const TopPanel = ({ numbers }: { numbers: number[][] }): JSX.Element => {
  return (
    <div className="toppanel">
      {numbers.map((line, idx) => (
        <div className="toppanel__line" key={idx}>
          {line.map((number, idx_2) => (
            <Cell value={number} key={idx_2} />
          ))}
        </div>
      ))}
    </div>
  );
};

