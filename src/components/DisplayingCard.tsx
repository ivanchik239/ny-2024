import React from 'react';
import pointer from '../icons/pointer.svg';
import './DisplayingCards.scss';
import classNames from 'classnames';

export const DisplayingCard = ({ value }: { value?: string | number }): JSX.Element => {
  switch (typeof value) {
    case 'number':
      return (
        <div className="cardContent">
          <div className={classNames('circleAround', 'number')}>{value}</div>
        </div>
      );
    case 'string':
      return (
        <div className="cardContent">
          <img src={value} alt="certificate" className="certificate" />
        </div>
      );
    default:
      return (
        <div className="cardContent">
          <div className="circleAround">
            <img src={pointer} alt="pointer" className="pointer" />
          </div>
        </div>
      );
  }
};

