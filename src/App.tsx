import { createContext, useContext, useEffect, useState } from 'react';
import './App.scss';
import big_snowflake_img from './icons/big_snowflake.png';
import Snowfall from 'react-snowfall';
import { RouterProvider } from 'react-router-dom';
import { router } from './pages';

export type nameType = 'mother' | 'father' | 'oma' | 'vika' | 'misha' | '';

export const PersonContext = createContext<{
  personValue: [value: nameType, setValue: (newValue: nameType) => void];
}>({
  personValue: ['', () => {}],
});

function App() {
  const [person, setPerson] = useState<nameType>('');

  const big_snowflake = new Image();
  big_snowflake.src = big_snowflake_img;
  const images = [big_snowflake];

  useEffect(() => {
    const personStorage = localStorage.getItem('person');
    if (personStorage?.length) {
      setPerson(personStorage as nameType);
    }
  }, []);

  return (
    <PersonContext.Provider value={{ personValue: [person, setPerson] }}>
      <div className="App">
        <RouterProvider router={router} />
        <Snowfall
          // Controls the number of snowflakes that are created (default 150)
          snowflakeCount={200}
          // Pass in the images to be used
          images={images}
          radius={[5, 10]}
          speed={[0.1, 0.5]}
          style={{ zIndex: -1 }}
        />
      </div>
    </PersonContext.Provider>
  );
}

export const usePerson = (): {
  personValue: [value: nameType, setValue: (newValue: nameType) => void];
} => {
  return useContext(PersonContext);
};

export default App;

