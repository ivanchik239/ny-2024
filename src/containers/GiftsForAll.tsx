import { useState } from 'react';
import ReactCardFlip from 'react-card-flip';
import { DisplayingCard } from '../components/DisplayingCard';
import './GiftsForAll.scss';
import { usePerson } from '../App';
import surfing from '../icons/surfing.jpeg';
import candles from '../icons/candles.jpeg';

type GiftsArray = Array<string | number>;

const misha: GiftsArray = [23, 99, 85, 69, 55];
const father: GiftsArray = [88, 22, 74];
const mother: GiftsArray = [39, 37, 54, 16, 91, candles];
const vika: GiftsArray = [28, 13, 11, 68, 41, surfing];
const oma: GiftsArray = [candles];

export const GiftsForAll = (): JSX.Element => {
  const {
    personValue: [name],
  } = usePerson();
  const [isFlipped, setIsFlipped] = useState(false);
  const [status, setStatus] = useState<'wait' | 'started' | 'finished'>('wait');

  const getValues = (): Array<string | number> => {
    switch (name) {
      case 'father':
        return father;
      case 'mother':
        return mother;
      case 'oma':
        return oma;
      case 'vika':
        return vika;
      case 'misha':
        return misha;
    }
    return [];
  };

  const values = getValues();
  const [iter, setIter] = useState(0);
  const [firstCard, setFirstCard] = useState<string | number | undefined>(undefined);
  const [secondCard, setSecondCard] = useState<string | number | undefined>(values[0]);

  const getDescription = (): string => {
    switch (isFlipped ? secondCard : firstCard) {
      case surfing:
        return ' серфинг!';
      case candles:
        return ' мастеркласс!';
      default:
        return '';
    }
  };

  const getTitle = (): string => {
    switch (status) {
      case 'wait':
        return 'Нажимай!';
      case 'started':
        return 'Твой подарок:' + getDescription();
      case 'finished':
        return '';
    }
  };

  const turnCard = (): void => {
    if (status !== 'started') {
      setStatus('started');
    }
    if (values[iter]) {
      if (isFlipped) {
        setFirstCard(values[iter]);
        setIter(iter + 1);
      } else {
        setSecondCard(values[iter]);
        setIter(iter + 1);
      }
    } else {
      setStatus('finished');
    }
    setIsFlipped(!isFlipped);
  };

  return (
    <div className="gifts">
      {status !== 'finished' ? (
        <>
          <div className="giftTitle">{getTitle()}</div>
          <div onClick={() => turnCard()} className="flip">
            <ReactCardFlip isFlipped={isFlipped} flipDirection="horizontal">
              <DisplayingCard value={firstCard} />
              <DisplayingCard value={secondCard} />
            </ReactCardFlip>
          </div>
        </>
      ) : (
        <div className="happyNY">
          {name === 'oma' && 'Еще подарки на мастерклассе.\n '}С Новым годом!
        </div>
      )}
    </div>
  );
};

