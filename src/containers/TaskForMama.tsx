import { useEffect, useRef, useState } from 'react';
import './Task.scss';
import Crossword, { ThemeProvider } from '@jaredreisinger/react-crossword';
import classNames from 'classnames';
import grinch from '../icons/grinch.webp';
import { useNavigate } from 'react-router-dom';
import { RootPaths } from '../pages';

export const TaskForMama = (): JSX.Element => {
  const navigate = useNavigate();
  const getInitialTimer = (): number => {
    if (localStorage.getItem('timer') !== null) {
      return parseInt(localStorage.getItem('timer') as string);
    } else {
      return 0;
    }
  };
  const [timer, setTimer] = useState<number>(getInitialTimer());
  const [waiting, setWaiting] = useState<boolean>(timer > 0);

  const ref = useRef(null);

  const checkCrossword = (): void => {
    // @ts-expect-error
    const isCrosswordCorrect = ref.current.isCrosswordCorrect();
    if (isCrosswordCorrect) {
      navigate(RootPaths.gift);
    } else {
      setWaiting(true);
      setTimer(3);
    }
  };

  useEffect(() => {
    if (timer > 0) {
      localStorage.setItem('timer', timer.toString());
      setTimeout(() => setTimer(timer - 1), 1000);
    } else {
      setWaiting(false);
      localStorage.removeItem('timer');
    }
  }, [timer]);

  return (
    <div className="task">
      <div className="task__title">Сначала реши кроссворд!</div>
      <ThemeProvider theme={theme}>
        <Crossword
          data={data}
          acrossLabel={'По горизонтали'}
          downLabel={'По вертикали'}
          ref={ref}
        />
      </ThemeProvider>
      <div className="task__warning">
        Будь внимателен: ты можешь проверить кроссворд только один раз за 120 секунд!
      </div>
      <button
        className={classNames(waiting && 'button-wait')}
        onClick={!waiting ? checkCrossword : () => {}}
      >
        {!waiting ? (
          'Проверить!'
        ) : (
          <div className="button-wait__content">
            <img src={grinch} alt="grinch" className="grinch" />
            <div>{timer}</div>
          </div>
        )}
      </button>
    </div>
  );
};

const data = {
  across: {
    1: {
      clue: 'Состояние больного диабетом, когда уровень глюкозы в крови слишком высок.',
      answer: 'ГИПЕРГЛИКЕМИЯ',
      row: 2,
      col: 4,
    },
    4: {
      clue: 'Частота сердечных сокращений увеличилась до 100 ударов в минуту. Как называется этот симптом?',
      answer: 'ТАХИКАРДИЯ',
      row: 5,
      col: 5,
    },
    7: {
      clue: 'Кровоизлияние по-другому',
      answer: 'ГЕМОРРАГИЯ',
      row: 8,
      col: 3,
    },
    9: {
      clue: 'У пациента проблемы с речью, нарушение походки, нарушение мимики и внезапная сильная головная боль. Скорее всего у него ...',
      answer: 'ИНСУЛЬТ',
      row: 10,
      col: 0,
    },
    10: {
      clue: 'Это все измеряли у ковидников',
      answer: 'САТУРАЦИЯ',
      row: 14,
      col: 0,
    },
    11: {
      clue: 'Этот синдром бывает двух видов, а возникает из-за врожденных дефектов развития ЦНС, наследственных заболеваний обмена, абсцессов и опухолей и некоторых болезнях крови',
      answer: 'СУДОРОЖНЫЙ',
      row: 13,
      col: 10,
    },
  },
  down: {
    2: {
      clue: 'Нарушение работы мозга, которое выражается в неспособности пациента оценивать происходящее и принимать здравые решения. (только первое слово)',
      answer: 'СПУТАННОСТЬ',
      row: 1,
      col: 6,
    },
    6: {
      clue: 'P-волна, интервал PQ, комплекс QRS, сегмент ST - это все показатели чего?',
      answer: 'ЭКГ',
      row: 6,
      col: 3,
    },
    8: {
      clue: 'Температура тела пациента опустилась до 34.7 градусов. Этот симптом называется ...',
      answer: 'ГИПОТЕРМИЯ',
      row: 7,
      col: 14,
    },
    12: {
      clue: 'Закрытые окна в квартире могут вызвать такое состояние.',
      answer: 'ГИПОКСИЯ',
      row: 8,
      col: 10,
    },
    5: {
      clue: 'Иногда это понятие называют лихорадкой',
      answer: 'ГИПЕРТЕРМИЯ',
      row: 4,
      col: 8,
    },
    3: {
      clue: 'Каково состояние организма при критическом снижении объема циркулирующей крови?',
      answer: 'ШОК',
      row: 0,
      col: 12,
    },
  },
};

const theme = {
  focusBackground: '#ffd84b',
  numberColor: 'black',
  gridBackground: 'transparent',
};

