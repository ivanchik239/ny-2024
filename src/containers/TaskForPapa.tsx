import { useEffect, useRef, useState } from 'react';
import './Task.scss';
import Crossword, { ThemeProvider } from '@jaredreisinger/react-crossword';
import classNames from 'classnames';
import grinch from '../icons/grinch.webp';
import { useNavigate } from 'react-router-dom';
import { RootPaths } from '../pages';

export const TaskForPapa = (): JSX.Element => {
  const navigate = useNavigate();
  const getInitialTimer = (): number => {
    if (localStorage.getItem('timer') !== null) {
      return parseInt(localStorage.getItem('timer') as string);
    } else {
      return 0;
    }
  };
  const [timer, setTimer] = useState<number>(getInitialTimer());
  const [waiting, setWaiting] = useState<boolean>(timer > 0);

  const ref = useRef(null);

  const checkCrossword = (): void => {
    // @ts-expect-error
    const isCrosswordCorrect = ref.current.isCrosswordCorrect();
    if (isCrosswordCorrect) {
      navigate(RootPaths.gift);
    } else {
      setWaiting(true);
      setTimer(120);
    }
  };

  useEffect(() => {
    if (timer > 0) {
      localStorage.setItem('timer', timer.toString());
      setTimeout(() => setTimer(timer - 1), 1000);
    } else {
      setWaiting(false);
      localStorage.removeItem('timer');
    }
  }, [timer]);

  return (
    <div className="task">
      <div className="task__title">Сначала реши кроссворд!</div>
      <ThemeProvider theme={theme}>
        <Crossword
          data={data}
          acrossLabel={'По горизонтали'}
          downLabel={'По вертикали'}
          ref={ref}
        />
      </ThemeProvider>
      <div className="task__warning">
        Будь внимателен: ты можешь проверить кроссворд только один раз за 120 секунд!
      </div>
      <button
        className={classNames(waiting && 'button-wait')}
        onClick={!waiting ? checkCrossword : () => {}}
      >
        {!waiting ? (
          'Проверить!'
        ) : (
          <div className="button-wait__content">
            <img src={grinch} alt="grinch" className="grinch" />
            <div>{timer}</div>
          </div>
        )}
      </button>
    </div>
  );
};

const data = {
  across: {
    2: {
      clue: 'Источник механической энергии, необходимой для движения автомобиля. Тепловая энергия преобразуется в механическую работу.',
      answer: 'ДВИГАТЕЛЬ',
      row: 3,
      col: 0,
    },
    4: {
      clue: 'Устройство, предназначенное для производства вещества, выработки электрической энергии или для создания электромагнитных, световых сигналов.',
      answer: 'ГЕНЕРАТОР',
      row: 7,
      col: 4,
    },
    7: {
      clue: 'Термостат в системе охлаждения выполняет роль ...',
      answer: 'КЛАПАНА',
      row: 11,
      col: 2,
    },
    11: {
      clue: 'Служит для ускорения прогрева холодного двигателя и автоматического регулирования его теплового режима в заданных пределах.',
      answer: 'ТЕРМОСТАТ',
      row: 14,
      col: 6,
    },
    12: {
      clue: 'Неисправность вызванная недостатком жидкости в системе охлаждения.',
      answer: 'ПЕРЕГРЕВ',
      row: 16,
      col: 9,
    },
  },
  down: {
    1: {
      clue: 'Узел, служащий для передачи тепла от охлаждающей жидкости потоку воздуха.',
      answer: 'РАДИАТОР',
      row: 0,
      col: 2,
    },
    3: {
      clue: 'Соединяет коленчатый вал двигателя с трансмиссией.',
      answer: 'СЦЕПЛЕНИЕ',
      row: 1,
      col: 6,
    },
    5: {
      clue: 'Деталь подвески автомобиля, предназначенная для поглощения вибраций и уменьшения передачи колесных неровностей на кузов.',
      answer: 'САЙЛЕНТБЛОК',
      row: 1,
      col: 10,
    },
    6: {
      clue: 'Узел, в котором осуществляется предварительная подготовка топливовоздушной смеси для автомобиля.',
      answer: 'КАРБЮРАТОР',
      row: 5,
      col: 8,
    },
    8: {
      clue: 'Массивное колесо, которое используется в современных двигателях как инерционный аккумулятор.',
      answer: 'МАХОВИК',
      row: 10,
      col: 4,
    },
    9: {
      clue: 'Устройство, которое запускает что-то.',
      answer: 'СТАРТЕР',
      row: 4,
      col: 12,
    },
    10: {
      clue: 'Служит для контроля за работой генератора.',
      answer: 'РЕЛЕРЕГУЛЯТОР',
      row: 4,
      col: 14,
    },
  },
};

const theme = {
  focusBackground: '#ffd84b',
  numberColor: 'black',
  gridBackground: 'transparent',
};

