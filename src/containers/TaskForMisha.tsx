/* eslint-disable react-hooks/exhaustive-deps */
import { useEffect, useState } from 'react';
import './Task.scss';
import classNames from 'classnames';
import grinch from '../icons/grinch.webp';
import { RootPaths } from '../pages';
import { useNavigate } from 'react-router-dom';

export const TaskForMisha = (): JSX.Element => {
  const navigate = useNavigate();
  const getInitialTimer = (): number => {
    if (localStorage.getItem('timer') !== null) {
      return parseInt(localStorage.getItem('timer') as string);
    } else {
      return 0;
    }
  };
  const [timer, setTimer] = useState<number>(getInitialTimer());
  const [waiting, setWaiting] = useState<boolean>(timer > 0);
  const [inputValue, setInputValue] = useState<string>('');

  const checkAnswer = (): void => {
    if (inputValue === '11') {
      navigate(RootPaths.gift);
    } else {
      setWaiting(true);
      setTimer(120);
    }
  };

  useEffect(() => {
    if (timer > 0) {
      localStorage.setItem('timer', timer.toString());
      setTimeout(() => setTimer(timer - 1), 1000);
    } else {
      setWaiting(false);
    }
  }, [timer]);

  return (
    <div className="task">
      <div className="task__title">Сначала реши задачу!</div>
      <div className="task__text">
        Слава и Сережа решили сыграть друг против друга в теннис. Они ставят 1 рубль на каждую
        сыгранную игру. Слава выиграл три ставки, а Сережа — 5 рублей. Сколько игр они сыграли?
      </div>
      <div className="task__warning">
        Будь внимателен: ты можешь ответить только один раз за 120 секунд!
      </div>
      <input type="number" value={inputValue} onChange={(e) => setInputValue(e.target.value)} />
      <div className="space" />
      <button
        className={classNames(waiting && 'button-wait')}
        onClick={!waiting ? checkAnswer : () => {}}
      >
        {!waiting ? (
          'Проверить!'
        ) : (
          <div className="button-wait__content">
            <img src={grinch} alt="grinch" className="grinch" />
            <div>{timer}</div>
          </div>
        )}
      </button>
    </div>
  );
};

