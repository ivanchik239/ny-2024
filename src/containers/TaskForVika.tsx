import { useEffect, useState } from 'react';
import './Task.scss';
import classNames from 'classnames';
import grinch from '../icons/grinch.webp';
import { JapaneseCrossword } from '../components/Japanese/JapaneseCrossword';
import _ from 'lodash';
import { useNavigate } from 'react-router-dom';
import { RootPaths } from '../pages';

export const TaskForVika = (): JSX.Element => {
  const navigate = useNavigate();
  const getInitialTimer = (): number => {
    if (localStorage.getItem('timer') !== null) {
      return parseInt(localStorage.getItem('timer') as string);
    } else {
      return 0;
    }
  };
  const [timer, setTimer] = useState<number>(getInitialTimer());
  const [waiting, setWaiting] = useState<boolean>(timer > 0);

  const field = [
    [0, 0, 0, 1, 0, 1, 0, 0, 0],
    [0, 0, 1, 0, 1, 0, 1, 0, 0],
    [0, 0, 0, 1, 1, 1, 0, 0, 0],
    [0, 1, 1, 1, 1, 1, 1, 1, 0],
    [0, 1, 0, 0, 1, 0, 0, 1, 0],
    [0, 1, 0, 0, 1, 0, 0, 1, 0],
    [0, 1, 0, 0, 1, 0, 0, 1, 0],
    [0, 1, 0, 0, 1, 0, 0, 1, 0],
    [0, 1, 1, 1, 1, 1, 1, 1, 0],
  ];

  const [currentField, setCurrentField] = useState(
    Array(field.length)
      .fill([])
      .map(() => Array(field[0].length).fill(0)),
  );

  const updateCell = (position: { x: number; y: number }): void => {
    const newField = _.cloneDeep(currentField);
    newField[position.x][position.y] += 1;
    if (newField[position.x][position.y] > 2) newField[position.x][position.y] = 0;
    setCurrentField(newField);
  };

  const checkAnswer = (): void => {
    let failed = false;
    field.forEach((line, line_id) =>
      line.forEach((elem, elem_id) => {
        if (elem === 1 && currentField[line_id][elem_id] !== 1) {
          setWaiting(true);
          setTimer(120);
          failed = true;
        }
      }),
    );
    if (!failed) {
      navigate(RootPaths.gift);
    }
  };

  useEffect(() => {
    if (timer > 0) {
      localStorage.setItem('timer', timer.toString());
      setTimeout(() => setTimer(timer - 1), 1000);
    } else {
      setWaiting(false);
      localStorage.removeItem('timer');
    }
  }, [timer]);

  return (
    <div className="task">
      <div className="task__title">
        Сначала реши японский кроссворд!{' '}
        <a href="https://japonskie.ru/rules" target="_blank" rel="noreferrer">
          Правила
        </a>
      </div>
      <div className="task__japanese">
        <JapaneseCrossword field={field} updateCell={updateCell} currentField={currentField} />
      </div>
      <div className="task__warning">
        Будь внимательна: ты можешь проверить решение только один раз за 120 секунд!
      </div>
      <div className="space" />
      <button
        className={classNames(waiting && 'button-wait')}
        onClick={!waiting ? checkAnswer : () => {}}
      >
        {!waiting ? (
          'Проверить!'
        ) : (
          <div className="button-wait__content">
            <img src={grinch} alt="grinch" className="grinch" />
            <div>{timer}</div>
          </div>
        )}
      </button>
    </div>
  );
};

