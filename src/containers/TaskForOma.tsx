import './Task.scss';

import { RootPaths } from '../pages';
import { Navigate } from 'react-router-dom';

export const TaskForOma = (): JSX.Element => {
  return <Navigate to={RootPaths.gift} />;
};

