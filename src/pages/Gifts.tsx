/* eslint-disable react-hooks/exhaustive-deps */
import React from 'react';
import { TaskForMisha } from '../containers/TaskForMisha';
import { usePerson } from '../App';
import { TaskForVika } from '../containers/TaskForVika';
import { TaskForPapa } from '../containers/TaskForPapa';
import { TaskForMama } from '../containers/TaskForMama';
import { Navigate } from 'react-router-dom';
import { TaskForOma } from '../containers/TaskForOma';

export const Gifts = (): JSX.Element => {
  const { personValue } = usePerson();
  const [person] = personValue;

  return <div className="grats">{puzzles[person]}</div>;
};

const puzzles: Record<string, JSX.Element> = {
  misha: <TaskForMisha />,
  vika: <TaskForVika />,
  father: <TaskForPapa />,
  mother: <TaskForMama />,
  oma: <TaskForOma />,
};

