import { GiftsForAll } from '../containers/GiftsForAll';
import { Gifts } from './Gifts';
import { RootPage } from './RootPage';
import { createBrowserRouter } from 'react-router-dom';

export const RootPaths = {
  root: '/',
  gift: '/gift',
  task: '/task',
};

export const router = createBrowserRouter([
  {
    path: RootPaths.root,
    element: <RootPage />,
  },
  {
    path: RootPaths.task,
    element: <Gifts />,
  },
  {
    path: RootPaths.gift,
    element: <GiftsForAll />,
  },
]);

