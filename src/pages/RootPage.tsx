/* eslint-disable react-hooks/exhaustive-deps */
import { useNavigate, useSearchParams } from 'react-router-dom';
import { RootPaths } from '.';
import { nameType, usePerson } from '../App';
import { useEffect } from 'react';

export const RootPage = (): JSX.Element => {
  const [searchParams, setSearchParams] = useSearchParams();
  const navigate = useNavigate();
  const { personValue } = usePerson();
  const [person, setPerson] = personValue;

  useEffect(() => {
    const newPerson = searchParams.get('person');
    if (newPerson?.length) {
      setPerson(newPerson as nameType);
      localStorage.setItem('person', newPerson);
      setSearchParams({});
    }
  }, []);

  return (
    <div className="grats">
      <div className="grats__title">
        <h1>С Новым</h1>
        <h1 className="year">2024</h1>
        <h1>годом!</h1>
      </div>
      <div className="greeting">{greetings[person]}</div>
      <div className="space" />
      <button className="takebtn" onClick={() => navigate(RootPaths.task)}>
        Получить подарок!
      </button>
    </div>
  );
};

const greetings: Record<string, string> = {
  mother:
    'С наступающим Новым Годом, мама! Пусть этот год принесет тебе огромное счастье и радость. Пусть каждый момент будет наполнен светом и уютом, а каждый день будет напоминанием о том, как важна для нас твоя любовь и поддержка.',
  father:
    'С Новым Годом, папа! Пусть этот год принесет тебе удовлетворение от достигнутых успехов, множество радостных моментов и невероятные впечатления. Пусть каждый день будет наполнен счастьем, здоровьем и любовью. С праздником!',
  oma: 'С 2024 годом, бабушка! Пусть эти волшебные праздники принесут в твою жизнь много тепла, радости и уюта. Пусть новый год заполнит дни счастьем, а каждый момент будет наполнен удивительными впечатлениями. Здоровья, силы и множество светлых моментов в грядущем году!',
  vika: 'С Новым Годом, Вика! Пусть грядущий год будет для тебя настоящим источником вдохновения! Пусть каждый день приносит новые возможности и радостные сюрпризы. Пусть удача сопутствует тебе во всех начинаниях, а жизнь радует своей красочностью. С наилучшими пожеланиями в новом году!',
  misha:
    'С Новым Годом, Миша! Пусть грядущий год станет для тебя временем удивительных открытий. Удачи в учебе, ярких впечатлений от игр и множества веселых моментов с друзьями. Пусть каждый день будет наполнен смехом и радостью! С 2024!',
};

